#define PLUGIN_AUTHOR ".#Zipcore"
#define PLUGIN_NAME "ZR: Auto Buy"
#define PLUGIN_VERSION "1.2.1"
#define PLUGIN_DESCRIPTION ""
#define PLUGIN_URL "zipcore.net"

#include <sourcemod>
#include <sdktools>
#include <sdkhooks>
#include <cstrike>
#include <zombiereloaded>
#include <multicolors>

int g_iAccount = -1;

public Plugin myinfo = 
{
	name = PLUGIN_NAME,
	author = PLUGIN_AUTHOR,
	description = PLUGIN_DESCRIPTION,
	version = PLUGIN_VERSION,
	url = PLUGIN_URL
}

bool g_bCheck[MAXPLAYERS + 1];
float g_fSpawnTime[MAXPLAYERS + 1];

public void OnPluginStart()
{
	CreateConVar("zr_autobuy_version", PLUGIN_VERSION, "ZR: Auto Buy version", FCVAR_DONTRECORD|FCVAR_PLUGIN|FCVAR_SPONLY|FCVAR_REPLICATED|FCVAR_NOTIFY);
	
	HookEvent("player_spawn", Event_Spawn);
	HookEvent("exit_buyzone", Event_ExitBuyZone);
	
	g_iAccount = FindSendPropInfo("CCSPlayer", "m_iAccount");
}

public Action Event_Spawn(Handle event, const char[] name, bool dontBroadcast)
{
	int client = GetClientOfUserId(GetEventInt(event, "userid"));
	
	g_bCheck[client] = true;
	g_fSpawnTime[client] = GetGameTime();
	
	return Plugin_Continue;
}

public Action Event_ExitBuyZone(Handle event, const char[] name, bool dontBroadcast)
{
	int client = GetClientOfUserId(GetEventInt(event, "userid"));
	
	if(!client)
		return Plugin_Continue;
	
	if(!g_bCheck[client])
		return Plugin_Continue;
	
	if(g_fSpawnTime[client]+1.0 > GetGameTime())
		return Plugin_Continue;
	
	g_bCheck[client] = false;
	
	if(!IsClientInGame(client))
		return Plugin_Continue;
		
	if(!IsPlayerAlive(client))
		return Plugin_Continue;
		
	if(!ZR_IsClientHuman(client))
		return Plugin_Continue;
		
	bool bGotWeapons;
	int iMoney = GetEntData(client, g_iAccount);
	int iMoneyBackup = iMoney;
		
	if(GetPlayerWeaponSlot(client, CS_SLOT_PRIMARY) == -1)
	{
		int iPrice = CS_GetWeaponPrice(client, CSWeapon_P90);
		if(iMoney >= iPrice)
		{
			iMoney -= iPrice;
			GivePlayerItem(client, "weapon_p90");
			bGotWeapons = true;
		}
	}
	
	if(GetPlayerWeaponSlot(client, CS_SLOT_SECONDARY) == -1)
	{
		int iPrice = CS_GetWeaponPrice(client, CSWeapon_ELITE);
		if(iMoney >= iPrice)
		{
			iMoney -= iPrice;
			GivePlayerItem(client, "weapon_elite");
			bGotWeapons = true;
		}
	}
	
	if(GetPlayerWeaponSlot(client, CS_SLOT_GRENADE) == -1)
	{
		int iPrice = CS_GetWeaponPrice(client, CSWeapon_INCGRENADE);
		if(iMoney >= iPrice)
		{
			iMoney -= iPrice;
			GivePlayerItem(client, "weapon_incgrenade");
			bGotWeapons = true;
		}
		
		iPrice = CS_GetWeaponPrice(client, CSWeapon_HEGRENADE);
		if(iMoney >= iPrice)
		{
			iMoney -= iPrice;
			GivePlayerItem(client, "weapon_hegrenade");
			bGotWeapons = true;
		}
		
		iPrice = CS_GetWeaponPrice(client, CSWeapon_FLASHBANG);
		if(iMoney >= iPrice)
		{
			iMoney -= iPrice;
			GivePlayerItem(client, "weapon_flashbang");
			bGotWeapons = true;
		}
		
		iPrice = CS_GetWeaponPrice(client, CSWeapon_FLASHBANG);
		if(iMoney >= iPrice)
		{
			iMoney -= iPrice;
			GivePlayerItem(client, "weapon_flashbang");
			bGotWeapons = true;
		}
	}
	
	if(GetPlayerWeaponSlot(client, CS_SLOT_KNIFE) == -1)
	{
		GivePlayerItem(client, "weapon_knife");
		bGotWeapons = true;
	}
	
	if(bGotWeapons)
	{
		if(iMoneyBackup != iMoney)
			SetEntData(client, g_iAccount, iMoney);
			
		CPrintToChat(client, "{darkred}[AutoBuy] {green}Now you're fully equipped.");
	}
	
	return Plugin_Continue;
}